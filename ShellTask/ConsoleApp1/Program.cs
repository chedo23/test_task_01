﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace shell
{
    class Program
    {
        static void Main(string[] args)
        {
            string createQuery = "Create";
            string DirName = "F:\\Новая папка Стасика";
            string DirRename = "F:\\Супер новая папка Стасика";
            string delQuery = "Delete";
            string renameQuery = "Rename";
            string result;
            string userQuery;
            string[] words;
            char[] charSeparators = { ' ' };

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Я Стасик, чё надо? Доступны команды: {createQuery} -полный_путь / {delQuery} / {renameQuery}");      //Заголовок
            do
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                try
                {
                    userQuery = Console.ReadLine().ToLower().Trim();   //передает сообщение клиента в переменную 
                    Console.CursorVisible = true;  
                    words = userQuery.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries); //разделяет переменную на строки массива,
                                                                                                    //массив разделителей указан в диклорации,
                                                                                                    //пустые строки не учитываются
                }
                catch (Exception)
                {
                    Console.WriteLine("Что-то пошло не так при вводе данных от пользователя");
                    Console.ReadLine();
                    continue;
                }

                switch (words.GetValue(0).ToString().Trim()) //получает первую запись в массиве, это должно быть что-то из кейсов
                {
                    case "create":
                        try
                        {
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            DirName = words.GetValue(1).ToString().Trim(); //получает вторую запись в массиве, это должен быть путь
                            Directory.CreateDirectory(DirName.Remove(0,1) ); //создает по указанному пути 
                            result = "Создал директорию!";
                            break;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Что-то пошло не так при Create");
                            Console.ReadLine();
                            continue;
                        }
                    case "delete":
                        try
                        {
                            Console.ForegroundColor = ConsoleColor.DarkMagenta;
                            Directory.Delete(DirName);
                            result = "Удалил папку!";
                            break;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Что-то пошло не так при Delete");
                            Console.ReadLine();
                            continue;
                        }
                    case "rename":
                        try
                        {
                            Console.ForegroundColor = ConsoleColor.DarkCyan; //TODO = Rename - НЕ РАБОТАЕТ СУКА
                            Directory.CreateDirectory(DirRename);
                            Directory.Move(DirName, DirRename);
                            result = "Переименовал папку!";
                            break;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Что-то пошло не так при Rename");
                            Console.ReadLine();
                            continue;
                        }
                    default:
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        result =
                            $"Команда не распознана. Воспользуйтесь доступными командами: {createQuery} / {delQuery} / {renameQuery}";
                        break;
                }

                Console.WriteLine(result);
                Console.WriteLine();
            } while (true);

        }
    }
}
